//
//  Walk.swift
//  Dog Walk
//
//  Created by Brandon Levasseur on 10/25/14.
//  Copyright (c) 2014 Razeware. All rights reserved.
//

import Foundation
import CoreData

class Walk: NSManagedObject {

    @NSManaged var date: NSDate
    @NSManaged var dog: Dog

}
