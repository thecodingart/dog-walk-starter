//
//  Dog.swift
//  Dog Walk
//
//  Created by Brandon Levasseur on 10/25/14.
//  Copyright (c) 2014 Razeware. All rights reserved.
//

import Foundation
import CoreData

class Dog: NSManagedObject {

    @NSManaged var name: String
    @NSManaged var walks: NSOrderedSet

}
